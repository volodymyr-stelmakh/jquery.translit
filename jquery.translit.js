/*!
 * jQuery Translite Plugin v1.1.3
 * http://knopix.net/
 *
 * Copyright 2014 Volodymyr Stelmakh (vov1)
 * Released under the MIT license
 */
(function( $ ){
	
	$.translit = function( text ) {
		// Specialchars replaced by
		var space = '-'; 
		var text = text.toLowerCase();	
		var symbols = {
			//numbers
			'0': '0', '1': '1', '2': '2', '3': '3', '4': '4', '5': '5', '6': '6', '7': '7', '8': '8', '9': '9',
			//en
			'a': 'a', 'b': 'b', 'c': 'c', 'd': 'd', 'e': 'e', 'f': 'f', 'g': 'g', 'h': 'h', 'i': 'i', 'j': 'j',
			'k': 'k', 'l': 'l', 'm': 'm', 'n': 'n', 'o': 'o', 'p': 'p', 'q': 'q', 'r': 'r', 's': 's', 't': 't',
			'u': 'u', 'v': 'v', 'w': 'w', 'x': 'x', 'y': 'y', 'z': 'z',
			// rus (ukr)
			'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh', 'з': 'z', 'и': 'i',
			'і': 'i', 'ї': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r',
			'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '',
			'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya',
			// symbols
			' ': space, '_': space, '`': space, '~': space, '!': space, '@': space, '#': space, '$': space,
			'%': space, '^': space, '&': space, '*': space, '(': space, ')': space, '-': space, '\=': space,
			'+': space, '[': space, ']': space, '\\': space, '|': space, '/': space, '.': space, ',': space,
			'{': space, '}': space, '\'': space, '"': space, ';': space, ':': space, '?': space, '<': space,
			'>': space, '№': space					
		}
		
		var result = '';
		var curent = '';
		
		for(i=0; i < text.length; i++) {
			// if symbol found - replace it
			if(symbols[text[i]] != undefined) {			
				if(curent != symbols[text[i]] || curent != space){
					result += symbols[text[i]];
					curent = symbols[text[i]];				
				}					
			}
		}	
		
		// Trim string	
		result = result.replace(/^-/, '');
		result = result.replace(/-$/, '');
	
		return result;						
	};

})( jQuery );
