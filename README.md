﻿# jquery.translit

Simple translit jQuery plugin.

## Installation

Include script *after* the jQuery library (unless you are packaging scripts somehow else):

```html
<script src="/path/to/jquery.translit.js"></script>
```

## Usage

Translit text:

```javascript
$.translit('Some text that you want to translit');
```

**It will return translited url friendly string**

## Example
```html
<input type="text" name="text" />
<input type="text" name="url" />
<button>Translit</button>

<script src="jquery.js"></script>
<script src="jquery.translit.js"></script>
<script>
<!--
$(document).ready(function () {
	$('button').click(function(){
		$('[name=url]').val( $.translit($('[name=text]').val()) );
	});
});
-->
</script>
```

## Authors

[Volodymyr Stelmakh (vov1)](https://bitbucket.org/vov1)